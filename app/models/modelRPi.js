module.exports = 
{
    getIP: getIP
}

function getIP(connection, callback)
{
    connection.query("select ip from instanceip limit 1", function(err, results)
    {
        if(err)
        {
            callback(err);
        }
        else
        {
            callback(null, results);
        }
    });
}