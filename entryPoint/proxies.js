var request = require('request');

module.exports = function (app, connection, ip) {

    console.log(ip);

    setInterval(function(){
        console.log("Fetching new IP")
        connection.query("select ip from instanceip limit 1", function(err, results){
            ip = results[0].ip;
        });
    }, 23400000);

    // Entry Point, both for local and Server
    app.get('/', function(req, res){
        console.log(req.route.path);
        request.get({url: 'http://'+ip+req.route.path}, function(error, response, body){
          res.send(body);  
        });
    });

    app.get('/aboutus', function(req, res){
        console.log(req.route.path);
        request.get({url: 'http://'+ip+req.route.path}, function(error, response, body){
          res.send(body);  
        });
    });

    app.get('/contactus', function(req, res){
        console.log(req.route.path);
        request.get({url: 'http://'+ip+req.route.path}, function(error, response, body){
          res.send(body);  
        });
    });

    // app.post('/contactus', cHome.contact);

    app.get('/signup', function(req, res){
        console.log(req.route.path);
        request.get({url: 'http://'+ip+req.route.path}, function(error, response, body){
          res.send(body);  
        });
    });

    // app.post('/signupcustomer', cSignUp.signupCustomer);

    // app.post('/signupcompany', cSignUp.signupCompany);

    // app.post('/signupagent', cSignUp.signupListingAgent);

    app.get('/dashboard', function(req, res){
        console.log(req.route.path);
        request.get({url: 'http://'+ip+req.route.path}, function(error, response, body){
          res.send(body);  
        });
    });

    // app.post('/signin', cSignIn.doSignIn);

    // app.post('/signinAleks', cSignIn.doSignInAleks);

    app.get('/logout', function(req, res){
        console.log(req.route.path);
        request.get({url: 'http://'+ip+req.route.path}, function(error, response, body){
          res.send(body);  
        });
    });

    app.get('/search', function(req, res){
        console.log(req.route.path);
        request.get({url: 'http://'+ip+req.route.path}, function(error, response, body){
          res.send(body);  
        });
    });

    app.get('/searchfinder', function(req, res){
        console.log(req.route.path);
        request.get({url: 'http://'+ip+req.route.path}, function(error, response, body){
          res.send(body);  
        });
    });

    app.get('/property', function(req, res){
        console.log(req.route.path);
        request.get({url: 'http://'+ip+req.route.path}, function(error, response, body){
          res.send(body);  
        });
    });

    // app.post('/addproperty', cDashboard.addProperty);

    // app.post('/uploadpropertyimage', cDashboard.uploadPropertyImage);

    // // Entrypoint for Password
    app.get('/loadforgotpassword',function(req, res){
        console.log(req.route.path);
        request.get({url: 'http://'+ip+req.route.path}, function(error, response, body){
          res.send(body);  
        });
    });

    // app.post('/forgotPassword', cSignIn.forgotPassword);

    // app.post('/resetpassword', cSignIn.resetPassword);

    // app.post('/deleteagent', cDashboard.removeListingAgent);

    // app.post('/message', cMessage.sendMessage);

    app.get('/customer-favorite', function(req, res){
        console.log(req.route.path);
        request.get({url: 'http://'+ip+req.route.path}, function(error, response, body){
          res.send(body);  
        });
    });

    app.get('/customer-favorite/remove', function(req, res){
        console.log(req.route.path);
        request.get({url: 'http://'+ip+req.route.path}, function(error, response, body){
          res.send(body);  
        });
    });

    // app.post("/uploaddp", cDashboard.uploadDP);

    // app.post('/uploaddptodb', cDashboard.saveDPToDB);

    // app.post('/editcustomer', cDashboard.editCustomer);

    // app.post('/reply', cMessage.replyMessage);

    // app.post('/getmsg', cMessage.getMessage);

    // app.get('/employees/:id/dashboard/:year/monthly-sales', cDashboard.getAgentMonthlySalesOfYear);

    // app.get('/employees/:id/dashboard/:year/monthly-sales-count', cDashboard.getAgentMonthlySalesCountOfYear);

    // app.get('/employees/:id/dashboard/yearly-total-sales', cDashboard.getAgentYearWiseTotalSales);

    // app.get('/employees/:id/dashboard/yearly-total-sales-count', cDashboard.getAgentYearWiseTotalSalesCount);

    // app.post('/editemployee', cDashboard.editEmployee);

    // app.post('/addsales', cDashboard.addSales);

    // app.post('/propertyprice', cDashboard.predictPrice);

    // //APIs for mobile app
    // app.post('/user/login', cAPI.authenticateUser);

    // app.post('/user/signup', cAPI.usersignup);
    
    app.get('/type/:typeName/values', function(req, res){
        console.log(req.route.path);
        request.get({url: 'http://'+ip+req.route.path}, function(error, response, body){
          res.send(body);  
        });
    });
    
    // app.post('/image', cAPI.uploadImage);
    
    app.get('/listing-agent/:id/properties', function(req, res){
        console.log(req.route.path);
        request.get({url: 'http://'+ip+req.route.path}, function(error, response, body){
          res.send(body);  
        });
    });
    
    app.get('/property/filter-options', function(req, res){
        console.log(req.route.path);
        request.get({url: 'http://'+ip+req.route.path}, function(error, response, body){
          res.send(body);  
        });
    });
    
    app.get('/property/search', function(req, res){
        console.log(req.route.path);
        request.get({url: 'http://'+ip+req.route.path}, function(error, response, body){
          res.send(body);  
        });
    });
    
    app.get('/property/:id', function(req, res){
        console.log(req.route.path);
        request.get({url: 'http://'+ip+req.route.path}, function(error, response, body){
          res.send(body);  
        });
    });
    
    // app.post('/property', cAPI.addProperty);

    // app.get('/getinstanceip', cAPI.getInstanceIP);
    
    // app.get('*', function(req, res)
    // {
    //     res.status(404).render('404');
    // });
}
